import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-ui-lib-example',
  template: `
    <p>
      ui-lib-example works!
    </p>
  `,
  styles: [
  ]
})
export class UiLibExampleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
