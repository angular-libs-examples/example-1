import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { UiLibExampleModule } from 'ui-lib-example';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    UiLibExampleModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
